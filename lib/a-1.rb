# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  ans_arr = []
  nums[0...-1].each_with_index do |num, index|
    old_num = num
    while old_num +1 != nums[index+1]
      old_num += 1
      ans_arr << old_num
    end
  end
  ans_arr
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  ans = 0
  binary = binary.chars.map(&:to_i)
  binary.each_with_index do |num,index|
    ans += num*(2**(binary.length-index-1))
  end
  ans
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)

  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  starting_arr = [-1,2,1]

  if n == 0
    return 2
  elsif n == 1
    return 1
  elsif n == -1
    return -1
  elsif n > 0
    ans = 1
    flag = 1
    while n > flag
      flag += 1
      ans = starting_arr[-2] + starting_arr[-1]
      starting_arr << ans
    end
    return ans
  else
    ans = -1
    flag = -1
    while n < flag
      flag -= 1
      ans = starting_arr[1] - starting_arr[0]
      starting_arr.unshift(ans)
    end
    return ans
  end
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  string = string.chars
  longest = 2
  longest_word = ""
  string[0...-2].each_with_index do |letter,index|
    if string[index] == string[index+1] #palindrome detected
      flag = 1
      step = 2
      lenz = 2
      while flag == 1 && index - step +1> 0 && index+step < string.length
        if string[index-step+1] == string[index+step]
          lenz += 2

          if lenz > longest
            longest = lenz
            longest_word = string[index-step+1..index+step]
          end
          step += 1
        else
          flag = 0
        end
      end
    elsif index > 0 && string[index-1] == string[index+1]
      flag = 1
      step = 2
      lenz = 3
      while flag == 1 && index - step > 0 && index+step < string.length
        if string[index-step] == string[index+step]
          lenz += 2

          if lenz > longest
            longest = lenz
            longest_word = string[index-step..index+step]
          end
          step += 1
        else
          flag = 0
        end
      end
    end
  end
  if longest_word != ""
    return longest
  else
    return false
  end
end
